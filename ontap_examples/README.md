# to install python requirements for Netapp.ontap

1) go to the repo for netapp.ontap
https://github.com/ansible-collections/netapp.ontap/tree/main

2) copy the contents of requirements.txt

3) recreate requirements.txt on your ansible box

4) run the following
pip install -r requirements.txt


